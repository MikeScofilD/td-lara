@section('day_content')
    <ul>
        @foreach($days as $day)
            @if ($day==date("d"))
                <li><span style="color: green">{{$day}}</span></li>
            @else
                <li><span style="color: yellowgreen">{{$day}}</span></li>
            @endif
        @endforeach
    </ul>
@endsection
