@section('header_content')
    <p>Aside Content</p>
    <table>
        @foreach($users as $user)
            <tr>
                <td>[Имя: {{$user['name']}}]</td>
                <td>[Фамилия: {{$user['surname']}}]</td>
                <td>
                    Статус: @if ($user['banned'])
                        <span style="color: green">Активен</span>
                    @else
                        <span style="color: red">Забанен</span>
                    @endif
                </td>
                <td><input type="submit" value="{{$user['name']}}"></td>
            </tr>
        @endforeach
    </table>
    <select>
        @foreach($users as $user)
            <option>[Имя: {{$user['name']}}]</option>
        @endforeach
    </select>
    <select>
        @foreach($users as $user)
            <option>[Имя: {{$user['surname']}}]</option>
        @endforeach
    </select>
@endsection
