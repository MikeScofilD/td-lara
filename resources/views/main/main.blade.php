@extends('layouts.app')

@section('title', 'Main Title')

@include('header.header', ['users' => $users])
@include('header.day', ['days' => $days])

@include('aside.aside', ['links' => $links])

@section('main_content')
    <p>Main Content</p>
@endsection

@include('footer.footer', ['employees' => $employees])
