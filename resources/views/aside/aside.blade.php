@section('aside_content')
    <p>Aside Content</p>
    <ul>
        @foreach($links as $link)
            <li><a href="{{$link['href']}}">{{$link['text']}}</a></li>
        @endforeach
    </ul>
@endsection
