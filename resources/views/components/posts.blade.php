@extends('layouts.post')
@section('main')
    <div class="post">
        <h2>тайтл страницы</h2>
        <div class="info">
            <span class="date">дата публикации</span>
            <span class="author">автор страницы</span>
        </div>
        <div class="text">
            короткое описание страницы
        </div>
        <div class="more">
            @foreach($posts as $id=>$post)
                <h3>{{$post['title']}}</h3>
                <p>{{$post['author']}}</p>
                <p>{{$post['date']}}</p>
                <p>{{$post['teaser']}}</p>
                <p>{{$post['text']}}</p>
                <a href="/post/{{$id}}">ссылка на пост</a>
            @endforeach
        </div>
    </div>
@endsection

