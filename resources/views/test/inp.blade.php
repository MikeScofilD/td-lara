<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
{{--Пусть в представлении даны 3 инпута.
 Передайте из действия в представление 3 переменные,
 значения которых запишите в атрибуты value наших инпутов. --}}

<form name="test" method="post" action="input1.php">
    <p><b>Ваше имя:</b><br>
        <input type="text" size="40" value="{{$name}}">
    </p>
    <p><b>Каким браузером в основном пользуетесь:</b><Br>
        <input type="radio" name="browser" value="ie"> Internet Explorer<Br>
        <input type="radio" name="browser" value="opera"> Opera<Br>
        <input type="radio" name="browser" value="firefox"> Firefox<Br>
    </p>
    <p>Комментарий<Br>
        <textarea name="comment" cols="40" rows="3" placeholder="{{$comment}}"></textarea></p>
    <p><input type="submit" value="Отправить">
        <input type="reset" value="Очистить"></p>
</form>
</body>
</html>
