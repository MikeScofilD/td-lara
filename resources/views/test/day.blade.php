<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
{{--Задача
Пусть из действия в представление передается номер дня недели.
 Сделайте так, чтобы, если передан номер воскресенья,
 то на экран вывелся текст 'Воскресенье'. Пусть текст выводится в абзаце.
--}}
@if ($day==7 || $day==6)
    <p>{{$day}} - Выходно</p>
@endif

{{--Модифицируйте предыдущую задачу так,
чтобы на экран выводилось названия дня недели в том случае,
если сегодня выходной день (то есть или суббота или воскресенье).
Для решения задачи напишите две директивы @if. --}}


{{--Задача--}}

{{--Передайте из действия в представление массив с числами. Выведите этот массив в виде списка ul.--}}
<ul>
    @foreach ($weeks as $week)
        <li>{{pow($week, 2)}}</li>
    @endforeach
</ul>

<ul>
    @foreach ($arr as $a)
        <li>{{$a}}</li>
    @endforeach
</ul>
<ul>
    @foreach ($users as $user)
        <li>{{$user['name']}}</li>
    @endforeach
</ul>
<ul>
    @foreach ($employees as $employee)
        <li>{{$employee['name']}} - {{$employee['surname']}} - {{$employee['salary']}}</li>
    @endforeach
</ul>
<ul>
    @foreach ($employees as $employee)
        @foreach ($employee as $emp)
            <li>{{$emp}}</li>
        @endforeach
    @endforeach
</ul>
</body>
</html>
