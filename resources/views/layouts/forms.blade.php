<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forms</title>
</head>
<body>
<h1>Forms</h1>
<form action="/form/save" method="post">
    @csrf
    <input type="text" name="text">
    {{--Модифицируйте предыдущую задачу так,
     чтобы квадрат переданного числа выводился в представлении над формой.  --}}
    {{--<p>{{$number}}</p>--}}
    <input type="number" name="number">
    <input type="submit">
</form>
<p>Введите 2 чиисла</p>
{{--<p>Sum={{$sum}}</p>--}}
<form action="/form/result" method="post">
    @csrf
    {{--Сделайте форму с двумя инпутами,
     в которые будут вводится числа.
     После отправки формы выведите на экран сумму этих чисел,
      а форму уберите.   --}}
    {{--<p>{{$number}}</p>--}}
    <input type="number" name="num1">
    <input type="number" name="num2">
    <input type="submit">
</form>
<form action="/task4" method="post">
    @csrf
    {{--Сделайте форму с двумя инпутами,
     в которые будут вводится числа.
     После отправки формы выведите на экран сумму этих чисел,
      а форму уберите.   --}}
    {{--<p>{{$number}}</p>--}}
    <input type="number" name="one">
    <input type="number" name="two">
    <input type="number" name="three">
    <input type="submit">
</form>
<form action="/form/pro" method="post">
    @csrf
    <input type="text" name="a">
    <input type="text" name="b">
    <input type="text" name="c">
    <input type="submit">
</form>
</body>
</html>
