<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update</title>
</head>
<body>
<form action="/show-user/{id}" method="post">
    @csrf
    <input type="text" name="name" value="{{$user->name}}">
    <input type="text" name="surname" value="{{$user->surname}}">
    <input type="number" name="age" value="{{$user->age}}">
    <input type="submit">
</form>
</body>
</html>
