<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employees</title>
</head>
<body>
<p>Employees</p>
{{--<p>{{$employees}}</p>--}}
@foreach($employees as $employee)
    {{--<p>id:{{$employee->id}} {{$employee->name}} {{$employee->birthday}} {{$employee->position}} ${{$employee->salary}}</p>--}}
    {{--<p>{{$employee->name}}</p>--}}
    <p>{{$employee->position}}</p>
    {{--Из таблицы employees получите массив имен работников.--}}
    {{--<p>{{$employee}}</p>--}}
@endforeach
</body>
</html>
