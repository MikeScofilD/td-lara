<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>Edit Post</p>
{{--<p>{{$post}}</p>--}}
<form action="/post/edited/{{$post->id}}" method="post">
    @csrf
    <input type="text" name="title" value="{{$post->title}}">
    <input type="text" name="description" value="{{$post->description}}">
    <input type="text" name="text" value="{{$post->text}}">
    <input type="date" name="date" value="{{$post->date}}">
    <input type="submit">
</form>
</body>
</html>
