<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Deleted Post</title>
</head>
<body>
@foreach($posts as $post)
    <p>Post id: {{$post->id}}</p>
    <p>title:{{$post->title}}</p>
    <p>desc:{{$post->desc}}</p>
    <p>text:{{$post->text}}</p>
    <p>date:{{$post->date}}</p>
    <a href="/post/restore/{{$post->id}}">Restore</a>
@endforeach
</body>
</html>
