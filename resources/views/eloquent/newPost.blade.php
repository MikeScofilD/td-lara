<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Post</title>
</head>
<body>
<p>New Post</p>
<form action="/post/store" method="post">
    @csrf
    <input type="text" name="title">
    <input type="text" name="description">
    <input type="text" name="text">
    <input type="date" name="date">
    <input type="submit">
</form>
</body>
</html>
