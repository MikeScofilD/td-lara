<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PostOne</title>
</head>
<body>
<p>Post id: {{$post->id}}</p>
<p>{{$post->title}}</p>
<p>{{$post->desc}}</p>
<p>{{$post->text}}</p>
<p>{{$post->date}}</p>
<a href="/post/new/">New Post</a>
<a href="/post/edit/{{$post->id}}">Edit</a>
<a href="/post/del/{{$post->id}}">Delete</a>
</body>
</html>
