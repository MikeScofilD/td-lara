<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TestController@v_main');
Route::get('/aside', 'TestController@v_aside');


//Сделайте так, чтобы при обращении на адрес /test/ в браузер выводилось сообщение '!'.
Route::get('/test', function () {
    return "!";
});

//Сделайте так, чтобы при обращении на адрес /dir/test/ в браузер выводилось сообщение '!!'.
Route::get('/dir/test', function () {
    return "!!";
});

//Пусть дан адрес вида /user/:id/,
// где вместо :id может быть любое число.
// Сделайте так,
// чтобы при обращении на адрес такого вида
// в браузер выводилось содержимое переданного параметра.
Route::get('/user/{id?}', function ($id = 'noname') {
    return $id;
});

//Пусть дан адрес вида /user/:name/,
// где вместо :name может быть любая строка.
// Сделайте так,
// чтобы при обращении на адрес такого вида в браузер выводилась строка 'имя юзера :name',
// где вместо :name будет переданная строка.

Route::get('/user/{name?}', function ($name = 'noname') {
    return "User name: {$name}";
});

//Пусть дан адрес вида /sum/:num1/:num2/,
// где вместо :num1 и :num2 могут быть любые числа.
// Сделайте так,
// чтобы при обращении на адрес такого вида в браузер выводилось сумма переданных чисел.

Route::get('/sum/{num1?}/{num2?}', function ($num1 = 0, $num2 = 0) {
    $res = $num1 + $num2;
    return "Sum = {$res}";
});

// Пусть дан адрес вида /user/show-:id/,
// где вместо :id может быть любое число.
// Сделайте так,
// чтобы при обращении на адрес такого вида в браузер выводилось содержимое переданного параметра.
Route::get('/user/show-{id?}', function ($id = 0) {
    return $id;
});

Route::get('/test/show/{param?}/{param2?}', 'TestController@show');

Route::get('/pages/show/{id?}', 'PageController@showOne');

Route::get('/pages/all', 'PageController@showAll');

Route::get('/test/sum/{num1}/{num2}/', 'TestController@sum');

Route::get('/employee/{id}', 'EmployeeController@showOne');
Route::get('/employee/{id}/{name}', 'EmployeeController@showField');

Route::get('/test/view', 'TestController@v_show');
Route::get('/test/view/{name}/{surname}', 'TestController@v_name_surname');

Route::get('/show/{name}/{age}/{salary}', 'TestController@v_nas');

Route::get('/input', 'TestController@v_input');
Route::get('/day/{day}', 'TestController@v_day');

//Route::get('/post/{id}', 'PostController@showOne');
Route::get('/posts', 'PostController@showAll');

Route::get('/product/{category_id}/{product_id}', 'ProductController@showProduct');

Route::get('/product/{category_id}', 'ProductController@showCategory');
Route::get('/product/categories', 'ProductController@showCategoryList');


Route::get('/form', 'TestFormController@form');
Route::post('/form/save', 'TestFormController@save');
Route::post('/form/result', 'TestFormController@result');
Route::post('/task4', 'TestFormController@task4');
Route::post('/form/pro', 'TestFormController@form_pro');

Route::get('/session', 'TestController@v_seesion');
Route::get('/session/get', 'TestController@v_get');

Route::get('test/show-form', 'TestController@show_form')->name('test.form');
Route::post('test/show-val', 'TestController@show_val')->name('test.show');

Route::get('text/plain', 'ResponseController@show');

//Route::get('/array', 'ArrayController');
Route::get('/array/arr/{n}', 'ArrayController@arr_n');
Route::get('/array/square/{n}', 'ArrayController@square');
Route::get('/array/sum/{n}', 'ArrayController@sum');
Route::get('/array/arr_unique', 'ArrayController@arr_unique');
Route::get('/array/minmax', 'ArrayController@max_min');
Route::get('/array/sort', 'ArrayController@arr_sort');
Route::get('/all', 'SQLController@allUsers');
Route::get('/add-user', 'SQLController@showUserForm');
Route::post('/save', 'SQLController@saveUser');
Route::get('/del/{id}', 'SQLController@delUser');

//Route::get('/show-one-user/{id}', 'SQLController@showOneUser');
Route::get('/show-form-user/{id}', 'SQLController@showUpdateFormUser');
Route::get('/show-user/{id}', 'SQLController@showUser');
Route::post('/show-user/{id}', 'SQLController@updateSaveUser');


//Employees
Route::get('/show-employees', 'EmployeeController@showEmployees');

//Events
Route::get('/show-events', 'EmployeeController@showEvents');

//Eloquent
Route::get('/show-posts', 'PostController@showPosts');

Route::get('/post/all', 'PostController@postAll')->name('postAll');
Route::get('/post/all/{order_field}', 'PostController@getAllOrder');
Route::get('/post/all/{order_field}/{order_type}', 'PostController@getAllOrderBy');
//Route::get('/post/{id}', 'PostController@postOne');
//Route::get('/post/{id}', 'PostController@postOneError');

//Изменение данных из моделей Eloquent
Route::get('/post/new/', 'PostController@newPost');
Route::post('/post/store', 'PostController@storePost');
Route::get('/post/action', 'PostController@actionPost');

Route::get('/post/edit/{id}', 'PostController@editPost');
Route::post('/post/edited/{id}', 'PostController@editedPost');


//Удаление данных из моделей Eloquent
Route::get('/post/del/{id}', 'PostController@deletedPost');
Route::get('/post/trashed', 'PostController@getDeletedPost');
Route::get('/post/restore/{id}', 'PostController@restorePost');

Route::get('/post/{id}', 'PostController@postOne')->name('postOne');




























