<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function PHPSTORM_META\map;

class TestController extends Controller
{
    // Теперь к нашему действию нужно будет обращаться не через адрес 'test/show',
    // а через адрес вида 'test/show/параметр', где вместо слова параметр может быть любая строка.
    // Переданный параметр будет попадать в параметр метода нашего действия:
    public function show($param = null, $param2 = null)
    {
        return "show() " . $param . ' ' . $param2;
    }
    // Задача
    //
    // Создайте контроллер Test,
    // а в нем действие sum.
    // Сделайте так, чтобы данное действие было доступно по адресу /test/sum/:num1/:num2/,
    // где где вместо :num1 и :num2 будут какие-нибудь числа.
    // Пусть по обращению к данному действию на экран выводится сумма переданных чисел.
    public function sum($num1, $num2)
    {
        return $num1 + $num2;
    }

    public function v_show()
    {
        return view('test.test', ['name' => "peter", 'surname' => 'peterere']);
    }
    // Пусть в действии контроллера даны переменные $name (имя), $surname (фамилия).
    // Передайте значения этих переменных в представление и выведите содержимое
    // каждой из этих переменных на экран.
    public function v_name_surname($name, $surname)
    {
        return view('test.test', ['name' => $name, 'surname' => $surname]);
    }
    // Задача
    //
    // Пусть в действии контроллера даны переменные $name (имя), $age (возраст) и $salary (зарплата).
    // Передайте значения этих переменных
    // в представление и выведите содержимое каждой из этих переменных в отдельном абзаце.
    public function v_nas($name = 0, $age = 0, $salary = 0)
    {
        // Задача
        //
        // Пусть в действии дана переменная, содержащая CSS класс.
        // Передайте эту переменную в представление
        // и для какого-нибудь тега значением атрибута class укажите нашу переменную.
        return view('test.show', ['name' => $name, 'age' => $age, 'salary' => $salary, 'red' => 'color:red']);
    }

    public function v_input()
    {
        return view('test.inp', ['name' => 'TestName', 'comment' => 'TestName']);
    }

    public function v_day($day)
    {
        $weeks = [1, 2, 3, 4, 5, 6];
        $arr = [
            'Июнь',
            '7',
            'Липень',
            'Июль',
            '8',
            'Серпень',
            'Август',
            '9',
            'Вересень',
            'Сентябрь',
        ];
        $users = [
            [
                'name' => 'user1',
                'age' => 21,
            ],
            [
                'name' => 'user2',
                'age' => 22,
            ],
            [
                'name' => 'user3',
                'age' => 23,
            ],
        ];
        $employees = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
        ];
        return view('test.day',
            ['day' => $day, 'weeks' => $weeks, 'arr' => $arr, 'users' => $users, 'employees' => $employees]);
    }

    public function v_main()
    {
        $links = [
            [
                'text' => 'text1',
                'href' => 'href1',
            ],
            [
                'text' => 'text2',
                'href' => 'href2',
            ],
            [
                'text' => 'text3',
                'href' => 'href3',
            ],
        ];
        $employees = [
            [
                'name' => 'Vasya',
                'surname' => 'Vasilev',
                'salary' => 1000,
            ],
            [
                'name' => 'Petia',
                'surname' => 'Petrovich',
                'salary' => 2000,
            ],
            [
                'name' => 'Masha',
                'surname' => 'Mashevch',
                'salary' => 3000,
            ],
            [
                'name' => 'Sasha',
                'surname' => 'Sashevoch',
                'salary' => 4000,
            ],
            [
                'name' => 'Anonimus',
                'surname' => 'Anon',
                'salary' => 5000,
            ],
        ];

        $employees = array_filter($employees, function ($item) {
            return $item['salary'] >= 2000;
        });

        $users = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'banned' => true,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'banned' => false,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'banned' => true,
            ],
            [
                'name' => 'user4',
                'surname' => 'surname4',
                'banned' => false,
            ],
            [
                'name' => 'user5',
                'surname' => 'surname5',
                'banned' => false,
            ],
        ];

        // Задача
        // Сделайте в действии контроллера массив с числами от 1 до последнего дня текущего месяца.
        // Передайте этот массив в представление. Сделайте также переменную,
        // в которой будет хранится номер текущего дня.
        // Также передайте эту переменную в представление.
        // Переберите циклом переданный массив и выведите его в виде списка ul.
        // При этом тегу li, в котором хранится номер текущего дня месяца добавьте класс active.
        $days = range(1, 31);

        return view('main.main', ['links' => $links, 'employees' => $employees, 'users' => $users, 'days' => $days]);
    }

    public function v_seesion(Request $request)
    {
        $request->session()->put('key', '123');
        //$request->session()->put(['hello'=>123]);
        //dd($request->session());

        return 'home';
    }

    public function v_get(Request $request)
    {
        dd($request->session()->all());
        return 'home';
    }

    public function sessionPut(Request $request)
    {
//        $request->session()->put('test', md5(rand(100, 1000000)));
        $request->session()->put('test', '123');
        return 'home';
    }

    public function sessionGet(Request $request)
    {
        dd($request->session()->all());

        return 'home';
    }

    public function show_form(Request $request)
    {
        //Задача
        //
        //Сделайте действие с формой,
        // в которой будет один инпут для ввода числа.
        // Пусть форма отправляется на это же действие.
        // Сделайте так, что если введено число от 1 до 10,
        // то выполнится редирект на другое действие,
        // представление которого выведет сообщение 'форма успешна отправлена'.
        //
        //Если же введено число не из разрешенного диапазона,
        // снова покажите пользователю форму,
        // написав над ней сообщение о том, что введено некорректное число.
        return view('layouts.red_from');
        //return redirect('test/show2');
    }

    public function show_val(Request $request)
    {
        $input = $request->input('number');
        //$input = intval($request->input('number'));
        //print_r($request->input('number'));
        //dd($request->input('number'));
        if ($input >= 1 && $input <= 10) {
            return 'форма успешна отправлена';
        } else {
//            return redirect('test/show-form');
            return redirect()->route('test.form');
        }
    }
}
