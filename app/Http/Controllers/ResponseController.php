<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;

//use Illuminate\Http\Response;

class ResponseController extends Controller
{
    public function show()
    {
//        return (new Response('Hello World', 200))->header('Content-Type', 'text/plain');
//        return response('Hello World', 200)->header('Content-Type', 'text/plain');
        return response('Hello World', 200)
            ->header('Content-Type', 'text/plain')
            ->header('X-Header-One', 'Header Value')
            ->header('X-Header-Two', 'Header Value');
    }
}
