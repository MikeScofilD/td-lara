<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $post;

    public function __construct()
    {
        $this->posts = [
            1 => [
                'title' => 'Тайтл страницы 1',
                'author' => 'Автор страницы 1',
                'date' => 'Дата публикации страницы 1',
                'teaser' => 'Короткое описание страницы 1',
                'text' => 'Полный текст страницы 1',
            ],
            2 => [
                'title' => 'Тайтл страницы 2',
                'author' => 'Автор страницы 2',
                'date' => 'Дата публикации страницы 2',
                'teaser' => 'Короткое описание страницы 2',
                'text' => 'Полный текст страницы 2',
            ],
            3 => [
                'title' => 'Тайтл страницы 3',
                'author' => 'Автор страницы 3',
                'date' => 'Дата публикации страницы 3',
                'teaser' => 'Короткое описание страницы 3',
                'text' => 'Полный текст страницы 3',
            ],
            4 => [
                'title' => 'Тайтл страницы 4',
                'author' => 'Автор страницы 4',
                'date' => 'Дата публикации страницы 4',
                'teaser' => 'Короткое описание страницы 4',
                'text' => 'Полный текст страницы 4',
            ],
            5 => [
                'title' => 'Тайтл страницы 5',
                'author' => 'Автор страницы 5',
                'date' => 'Дата публикации страницы 5',
                'teaser' => 'Короткое описание страницы 5',
                'text' => 'Полный текст страницы 5',
            ],
        ];
    }

    public function showOne($id)
    {
        //Задача
        //В контроллере Post сделайте действие showOne,
        // маршрут к которому будет следующий: /posts/:id/,
        // где :id будет представлять собой номер поста в массиве $this->posts.
        // Данное действие должно будет выводить запрошенный пост (все поля, кроме короткого описания).
        $view = 'post';

        if (!in_array($id, array_keys($this->posts))) {
            $view = 'page_undefined';
        }
        return view('components.' . $view, [
            'id' => $id,
            'post' => isset($this->posts[$id]) ? $this->posts[$id] : ""
        ]);


    }

    public function showAll()
    {
        // В контроллере Post сделайте действие showAll, маршрут к которому будет следующий: /posts/.
        //
        // Данное действие должно показывать на экран список всех постов с их тайтлами и короткими описаниями,
        // но без полного текста. Каждый пост должен иметь ссылку на свое полное описание
        // (то есть на страницу действия showOne для этого поста).
        return view('components.posts', ['posts' => $this->posts]);
    }

    //Введение в Eloquent Laravel

    //Задача 1
    //Сделайте через PhpMyAdmin таблицу posts (статьи)
    // с полями id, title (заголовок), desc (короткое описание статьи),
    // text (текст статьи), date (дата публикации). Добавьте в эту таблицу 5 статей

    //Задача 2
    //Сделайте модель Eloquent для таблицы posts из предыдущей задачи.

    public function showPosts()
    {
        return "hello";
    }

    // Задача 2
    // В контроллере PostController сделайте действие getAll
    // для получения списка всех статей.
    // Пусть это действие будет доступно по адресу /post/all/.
    public function postAll()
    {
        //Задача 3
        //Используя модель Post,
        // созданную в предыдущем уроке для таблицы posts,
        // получите в действии getAll массив всех статей,
        // передайте его в представление и выведите циклом на экран в виде HTML таблицы с колонками
        // id, title (заголовок) и desc (то есть без текста статьи).
//        $posts = Post::all();
        //Задача 6
        //Отредактируйте представление действия getAll так,
        // чтобы заголовок каждой из выводимых статей стал ссылкой на соответствующую страницу одной статьи.

        //Задача 7
        //Отредактируйте действие getAll так,
        // чтобы получаемые статьи были отсортированы по убыванию даты публикации.
        $posts = Post::orderBy('date', 'desc')->get();
        return view('eloquent.posts', ['posts' => $posts]);
    }

    //Задача 4
    //В контроллере PostController
    // сделайте действие getOne для получения одной статьи по ее id.
    // Пусть это действие будет доступно по адресу /post/:id/,
    // где :id представляет собой id желаемой записи.
    //Не забудьте наложить ограничение регулярным выражением на наш параметр.

    //Задача 5
    //В действии getOne из таблицы posts
    // получите статью соответствующую переданному параметру.
    // Передайте полученную статью в представление и выведите ее на экран,
    // оформив соответствующим HTML кодом.
    public function postOne($id)
    {
        $post = Post::find($id);

        return view('eloquent.postOne', ['post' => $post]);
    }

    //Задача 8
    //Отредактируйте маршрут действия getAll так,
    // чтобы вместо адреса /post/all/ нашей действие стало доступно по адресу /post/all/:order/,
    // где :order представляет собой имя поля, по которому выполнять сортировку.
    //Сделайте так, чтобы сортировать можно было по полям id, title и date.
    // Причем сортировка должна быть по убыванию значения поля.
    //Сделайте так, чтобы параметр :order был не обязательным и по умолчанию имел значение date.
    public function getAllOrder(string $order_field)
    {
        $post = Post::orderBy($order_field)->get();
        dd($post);
        return "hello";
    }
    //Задача 9
    //Отредактируйте маршрут действия getAll так,
    // чтобы появился еще один параметр :dir,
    // представляющий собой направление сортировки (по убыванию или по возрастанию).
    // То есть наш маршрут станет выглядеть так: /post/all/:order/:dir.
    //Пусть параметр :dir может иметь только два значения: asc или desc.
    // При этом пусть наш параметр также является не обязательным и по умолчанию имеет значение desc.
    public function getAllOrderBy(string $order_field, string $order_typ)
    {
        $post = Post::orderBy($order_field, $order_typ)->get();
        dd($post);
    }
    //Задача 10
    //Отредактируйте действия getOne так, чтобы,
    // если для переданного параметром id не существует записи в базе данных, то выводилась ошибка 404.

    public function postOneError($id)
    {
        $post = Post::findOrFail($id);

        return view('eloquent.postOne', ['post' => $post]);
    }

    //Изменение данных из моделей Eloquent

    //Задача 1
    // В контроллере PostController, созданном в предыдущем уроке, сделайте действие newPost для создания новой статьи.
    // Пусть это действие будет доступно по адресу /post/new/.
    // В представлении действия покажите форму для добавления новой записи.
    // После отправки формы сохраните новую запись.

    public function newPost()
    {
        return view('eloquent.newPost');
    }

    public function storePost(Request $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->text = $request->text;
        $post->date = $request->date;
        $post->save();
        return redirect()->route('postAll');

//        dd($post->title, $post->description, $post->text, $post->date);
        //dd($request->title,$request->description, $request->text, $request->date);
    }

    //Задача 2
    //Сделайте какое-нибудь действие, по заходу на которое для статьи с id 1 поменяйте title и короткое описание desc.

    public function actionPost()
    {
        $post = Post::find(2);
        $post->title = 'Moscow';
        $post->save();
        return redirect()->route('postAll');
    }

    //Задача 3
    //Сделайте самостоятельно описанное мною редактирование статьи.
    public function editPost($id)
    {
        $post = Post::find($id);
        return view('eloquent.editPost', ['post' => $post]);
    }

    public function editedPost(Request $request, $id)
    {
        $post = Post::find($id);
        $post->title = $request->title;
        $post->description = $request->description;
        $post->text = $request->text;
        $post->date = $request->date;
        $post->save();
        return redirect()->route('postOne', ['id' => $id]);
    }

    //Задача 4
    //Отредактируйте представление действия getAll так,
    // чтобы появилась еще одна колонка со ссылкой на редактирование соответствующей статьи.

    //Задача 5
    //Модифицируйте код действия editPost так, чтобы после сохранения формы выполнялся редирект на список всех записей (то есть на действие getAll).


    //Задача 6
    //Модифицируйте предыдущую задачу так, чтобы при редиректе отправлялось флеш сообщение об успешном обновлении записи.
    // Выводите это сообщение в представлении действия getAll.

    //Задача 7
    //Модифицируйте предыдущую задачу так, чтобы во флеш сообщении был указан id и title статьи, подвергнувшейся изменению.

    //Задача 8
    //Самостоятельно опробуйте изученную теорию.


    //Удаление данных из моделей Eloquent

    //Задача 1
    //В контроллере PostController, созданном в предыдущих уроках, сделайте действие delPost для удаления статьи.
    //Пусть это действие будет доступно по адресу /post/del/:id, где :id будет содержать id статьи для удаления.

    public function deletedPost($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect()->route('postAll');
    }

    //Задача 2
    //Отредактируйте представление действия getAll так, чтобы появилась еще одна колонка со ссылкой на удаления соответствующей статьи.
    //После удаления выполняйте редирект обратно на список статей с флеш сообщением об успешном удалении статьи.
    // Сообщение должно содержать title удаленной статьи.


    //Мягкое удаление
    //Задача 3
    //Реализуйте мягкое удаление статей.

    //Задача 4
    //Сделайте действие getDeletedPost, выводящее список удаленных статей.
    public function getDeletedPost()
    {
        $posts = Post::onlyTrashed()->get();
//        dd($posts);
        return view('eloquent.deletedPost', ['posts' => $posts]);
    }

    //Задача 5
    //Сделайте действие restorePost, восстанавливающее удаленную статью обратно.
    public function restorePost($id)
    {
        $posts = Post::onlyTrashed()->where('id', $id)->restore();
        return redirect()->route('postAll');
    }

    //Задача 6
    //В представлении действия getDeletedPost для каждой статьи сделайте ссылку на ее восстановление.

}
