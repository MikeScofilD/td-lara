<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestFormController extends Controller
{
    public $number;
    public $sum;

    public function __construct()
    {
        $this->number = 0;
        $this->sum = 0;
    }

    public function form(Request $request)
    {
//        if ($request->has('text')) {
//            var_dump($request->input('text'));
//        }
        // Сделайте форму с инпутом,
        // принимающим своим значением число.
        // После отправки формы выведите через var_dump квадрат этого числа.
//        if ($request->has('number')) {
//            var_dump($request->input('number') * $request->input('number'));
//            $this->number = $request->input('number') * $request->input('number');
//        }
//
//        if ($request->has('num1') && $request->has('num2')) {
//            var_dump($request->input('num1') + $request->input('num2'));
//            $this->sum = $request->input('num1') + $request->input('num2');
//        }
//        return view('layouts.forms', ['number' => $this->number, 'sum' => $this->sum]);
        return view('layouts.forms');
    }

    public function save(Request $request)
    {
        return $request->input('number');
    }

    public function result(Request $request)
    {
        return $this->sum = $request->input('num1') + $request->input('num2');
    }

    public function task4(Request $request)
    {
        if ($request->has('one') && $request->has('two') && $request->has('three')) {
            return $request->input('one') . " " . $request->input('two') . " " . $request->input('three');
        }
        return "error";
        return $this->result();
    }

    public function form_pro(Request $request)
    {
        $values = $request->except('_token');
        $only = $request->only('a', 'b');
        $path = $request->getUri();
        $full = $request->url();
        $url = $request->fullUrl();
        $is = $request->is();
        return view('layouts.form_pro', [
            'form_all' => $values,
            'only' => $only,
            'path' => $path,
            'full' => $full,
            'url' => $url,
            'is' => $is,
        ]);
    }
}
