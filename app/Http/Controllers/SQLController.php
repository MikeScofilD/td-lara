<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SQLController extends Controller
{
    //Задача 1
    //
    // Сделайте через PhpMyAdmin таблицу users
    // с полями id, name (имя), surname (фамилия), age (возраст).
    // Добавьте в эту таблицу 5 юзеров.

    //Задача 2
    //
    //С помощью фасада DB получите всех юзеров,
    // возраст которых более 18 лет.
    // Передайте полученных юзеров в представление и выведите их в виде таблицы table.
    public function allUsers()
    {
        $users = DB::select('select * from users where age>18');
        return view('layouts.users', ['users' => $users]);
    }

    //Задача 3
    //
    //С помощью фасада DB добавьте нового юзера в таблицу users.
    public function showUserForm()
    {
        return view('layouts.addUser');
    }

    /**
     * Save user data from Form
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveUser(Request $request)
    {
        if ($request->has('name') && $request->has('surname') && $request->has('age')) {
            DB::table('users')->insert(
                [
                    'name' => $request->input('name'),
                    'surname' => $request->input('surname'),
                    'age' => $request->input('age'),
                ]
            );
        }
        return redirect()->to('/all');
    }
    //Задача 4
    //
    //С помощью фасада DB удалите какого-нибудь нового юзера из таблицы users.
    public function delUser($id)
    {
        DB::table('users')->delete(
            [
                'id' => $id,
            ]
        );
        return "You Delete id = " . $id;
    }
    //Задача 5
    //
    //С помощью фасада DB поменяйте имя и фамилию какому-нибудь юзеру из таблицы users.
//    public function showOneUser($id) //show-one-user/{id}
//    {
//        $user = DB::table('users')->where('id', $id)->first();
//        return view('layouts.showOneUser', ['user' => $user]);
//    }
    public function showUpdateFormUser($id) //show-form-user/{id}
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('layouts.showFormUser', ['user' => $user]);
    }

    public function showUser($id) //show-user/{id}
    {
        $user = DB::table('users')->where('id', $id)->first();

        //update users set name = Olya, surname = Olegovna, age = 32 where id='id';
        return view('layouts.showOneUser', ['user' => $user]);
    }

    public function updateSaveUser(Request $request, $id) //update-save-user/{id}
    {
        $user = DB::table('users')->where('id', $id)->first();
        DB::table('users')->update(
            [
                'name' => $request->input('name'),
                'surname' => $request->input('surname'),
                'age' => $request->input('age'),
            ]
        );
        return view('layouts.showOneUser', ['user' => $user]);
    }
}
