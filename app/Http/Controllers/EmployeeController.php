<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    //Создайте контроллер Employee (работник).
    // В конструкторе класса запишите в свойство $this->employees массив с работниками:
    private $employees;

    public function __construct()
    {
        $this->employees = [
            1 => [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            2 => [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            3 => [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
            4 => [
                'name' => 'user4',
                'surname' => 'surname4',
                'salary' => 4000,
            ],
            5 => [
                'name' => 'user5',
                'surname' => 'surname5',
                'salary' => 5000,
            ],
        ];
    }
    //В контроллере Employee сделайте действие showOne.
    // Параметром оно должно принимать номер работника в массиве $this->employees
    // и выводить на экран строку с именем, фамилией и зарплатой работника.
    public function showOne($id)
    {
        return $this->employees[$id]['name'] . ' ' . $this->employees[$id]['surname'] . ' ' . $this->employees[$id]['salary'];
    }
    //В контроллере Employee сделайте действие showField.
    // Параметрами оно должно принимать номер работника в массиве $this->employees
    // и название одного из полей ('name', 'surname' или 'salary').
    // Действие выводить на экран значение соответствующего поля соответствующего работника.
    public function showField($id, $param)
    {
        return $this->employees[$id][$param];
    }

    //Задача 2
    //Получите все записи из таблицы employees и выведите их в представлении в виде таблицы table.
    public function showEmployees() //show-employees
    {
//        $employees = DB::table('employees')->get();

        //Задача 3
        //Модифицируйте предыдущую задачу так,
        // чтобы запрос получал только поле с именем и поле с зарплатой работника.
        //$employees = DB::table('employees')->select('name', 'salary')->get();

        //Задача 4
        //Из таблицы employees получите всех работников с зарплатой 500.
        //$employees = DB::table('employees')->where('salary', '=', 500)->get();

        //Задача 5
        //Из таблицы employees получите всех работников с зарплатой более 450.
        //$employees = DB::table('employees')->where('salary', '>=', 450)->get();

        //Задача 6
        //Из таблицы employees получите всех работников с зарплатой, не равной 500.
        //$employees = DB::table('employees')->where('salary', '!=', 500)->get();

        //Задача 7
        //Из таблицы employees получите всех работников с зарплатой 400 или id, большем 4.
        //$employees = DB::table('employees')->where('salary', '=', 400)->orWhere('id', '>', 4)->get();

        //Задача 8
        //Из таблицы employees получите работника с id, равным 3.
        //$employees = DB::table('employees')->where('id', '=', 3)->get();

        //Задача 9
        //Из таблицы employees получите работника имя работника id, равным 5.
        //$employees = DB::table('employees')->where('id', '=', 5)->value('name');

        //Задача 10
        //Из таблицы employees получите массив имен работников.
        //$employees = DB::table('employees')->pluck('name');

        //Задача 11
        //Из таблицы employees получите работников, зарплата которых находится в промежутке от 450 до 1100.
        //$employees = DB::table('employees')->whereBetween('salary', [450, 1100])->get();

        //Задача 12
        //Из таблицы employees получите работников, зарплата которых находится НЕ в промежутке от 300 до 600.
        //$employees = DB::table('employees')->whereNotBetween('salary', [300, 600])->get();

        //Задача 13
        //Из таблицы employees получите работников с id, равными 1, 2, 3 и 5.
        //$employees = DB::table('employees')->whereIn('id', [1,2,3,5])->get();

        //Задача 14
        //Из таблицы employees получите работников с id, НЕ равными 1, 2, 3.
        //$employees = DB::table('employees')->whereNotIn('id', [1,2,3])->get();

        //Задача 15
        //Из таблицы employees получите работников, у которых зарплата id от 1 до 3, либо зарплата от 400 до 800.
        //$employees = DB::table('employees')->whereBetween('id', [1, 3])->orWhereBetween('salary', [400, 800])->get();

        //Задача 16
        //Из таблицы employees получите работников, у которых зарплата равна 500 ИЛИ должность 'программист'.
        //$employees = DB::table('employees')->where('position', '=', 'программист')->orWhere('salary', '=', 500)->get();

        //Задача 17
        //Из таблицы employees получите работников, у которых зарплата равна 500 И должность 'программист'.
        //$employees = DB::table('employees')->where('position', '=', 'программист')->where('salary', '=', 500)->get();

        //Задача 20
        //Из таблицы employees получите всех работников и отсортируйте их по возрастанию зарплаты.
        //$employees = DB::table('employees')->orderBy('salary', 'asc')->get();

        //Задача 21
        //Из таблицы employees получите всех работников и отсортируйте их по убыванию даты рождения.
        //$employees = DB::table('employees')->orderBy('birthday', 'desc')->get();

        //Задача 22
        //Из таблицы employees получите максимальную зарплату.
        //$employees = DB::table('employees')->max('salary');

        //Задача 23
        //Из таблицы employees получите суммарную зарплату всех работников.
        //$employees = DB::table('employees')->sum('salary');

        //Задача 24
        //Из таблицы employees для каждой должности получите минимальную зарплату.
        //SELECT position, MIN(salary) FROM employees GROUP BY position;
        $employees = DB::table('employees')->selectRaw('position, MIN(salary)')->groupBy('position')->get();
//        dd($employees);
        //Задача 25
        //Из таблицы employees для каждой должности получите суммарную зарплату.

        //Задача 26
        //
        //Из таблицы employees получите работника, у которого день рождения приходится на дату 1988-03-25.

        //Задача 27
        //
        //Из таблицы employees получите работников, у которых день рождения приходится на 25 день месяца.

        //Задача 28
        //
        //Из таблицы employees получите работников, у которых день рождения в марте.

        //Задача 29
        //
        //Из таблицы employees получите работников, родившихся в 1990 году.


        return view('layouts_employees.show_employees', ['employees' => $employees]);
    }

    public function showEvents() //show-events
    {
        //Задача 18
        //Сделайте таблицу events (мероприятия), с полями id, name (название), start (начало мероприятия), finish (конец мероприятия).
        //$events = DB::table('events')->get();
        //Задача 19
        //Из таблицы events получите мероприятия, у которых дата начала и дата конца приходится на один и тот же день.
        $events = DB::table('events')->whereColumn('start', 'finish')->get();
        return view('layouts_employees.show_events', ['events' => $events]);
    }

}
