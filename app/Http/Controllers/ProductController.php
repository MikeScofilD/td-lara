<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->categories = [
            1 => [
                'name' => 'Категория 1',
                'products' => [
                    1 => [
                        'name' => 'Продукт 1',
                        'cost' => '300',
                        'inStock' => true,
                        'desc' => 'Описание продукта 1'
                    ],
                    2 => [
                        'name' => 'Продукт 2',
                        'cost' => '400',
                        'inStock' => true,
                        'desc' => 'Описание продукта 2'
                    ],
                    3 => [
                        'name' => 'Продукт 3',
                        'cost' => '500',
                        'inStock' => false,
                        'desc' => 'Описание продукта 3'
                    ],
                ],
            ],
            2 => [
                'name' => 'Категория 2',
                'products' => [
                    1 => [
                        'name' => 'Продукт 1',
                        'cost' => '700',
                        'inStock' => true,
                        'desc' => 'Описание продукта 1'
                    ],
                    2 => [
                        'name' => 'Продукт 2',
                        'cost' => '800',
                        'inStock' => false,
                        'desc' => 'Описание продукта 2'
                    ],
                    3 => [
                        'name' => 'Продукт 3',
                        'cost' => '900',
                        'inStock' => false,
                        'desc' => 'Описание продукта 3'
                    ],
                ],
            ],
        ];
    }

    public function showProduct(int $category_id, int $product_id)
    {
        //Задача
        //
        //В контроллере Product сделайте действие showProduct,
        // маршрут к которому будет следующий: /product/:category_id/:product_id,
        // где :category_id будет представлять собой номер категории в массиве $this->categories,
        // а :product_id - номер продукта в подмассиве products соответствующей категории.
        //
        //Данное действие должно будет выводить запрошенный продукт с названием,
        // ценой, наличием на складе, описанием продукта и названием категории этого продукта.
        //
        //Пусть наличие на складе выводится либо строкой 'есть в наличии', либо строкой 'нет в наличии'.
        //dd($this->categories);
        return "Имя: " . $this->categories[$category_id]['products'][$product_id]['name']
            . "<br>Цена: " . $this->categories[$category_id]['products'][$product_id]['cost']
            . "<br>Описание: " . $this->categories[$category_id]['products'][$product_id]['desc'];

        //Отредактируйте представление действия showProduct так,
        // чтобы название категории продукта было ссылкой на список продуктов данной категории.
    }

    public function showCategory($category_id)
    {
        // В контроллере Product сделайте действие showCategory,
        // маршрут к которому будет следующий: /product/:category_id/, где :category_id
        // будет представлять собой номер категории в массиве $this->categories.

        //Данное действие должно будет выводить список продуктов данной категории.
        // Пусть в списке будет название продукта и цена.

        //Название продукта должно быть ссылкой на страницу одного продукта
        // (то есть на действие showProduct).
//        dd($this->categories[$category_id]);
//        dd($this->categories[$category_id]['products']);
        return view('layouts.products', ['categories' => $this->categories]);
    }

    public function showCategoryList()
    {
        // В контроллере Product сделайте действие showCategoryList,
        // маршрут к которому будет следующий: /categories/.
        //Данное действие должно будет выводить список всех категорий.
        // Пусть выводится название категории и количество продуктов в этой категории.
        //Название категории должно быть ссылкой на список продуктов данной категории.
        return view('layouts.products', ['categories' => $this->categories]);
    }
}
